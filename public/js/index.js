"use strict";

const DATA = [
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m1.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "8 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f1.png",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m2.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Тетяна",
		"last name": "Мороз",
		photo: "./img/trainers/trainer-f2.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
	},
	{
		"first name": "Сергій",
		"last name": "Коваленко",
		photo: "./img/trainers/trainer-m3.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
	},
	{
		"first name": "Олена",
		"last name": "Лисенко",
		photo: "./img/trainers/trainer-f3.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
	},
	{
		"first name": "Андрій",
		"last name": "Волков",
		photo: "./img/trainers/trainer-m4.jpg",
		specialization: "Бійцівський клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
	},
	{
		"first name": "Наталія",
		"last name": "Романенко",
		photo: "./img/trainers/trainer-f4.jpg",
		specialization: "Дитячий клуб",
		category: "спеціаліст",
		experience: "3 роки",
		description:
			"Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
	},
	{
		"first name": "Віталій",
		"last name": "Козлов",
		photo: "./img/trainers/trainer-m5.jpg",
		specialization: "Тренажерний зал",
		category: "майстер",
		experience: "10 років",
		description:
			"Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
	},
	{
		"first name": "Юлія",
		"last name": "Кравченко",
		photo: "./img/trainers/trainer-f5.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
	},
	{
		"first name": "Олег",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-m6.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "12 років",
		description:
			"Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
	},
	{
		"first name": "Лідія",
		"last name": "Попова",
		photo: "./img/trainers/trainer-f6.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
	},
	{
		"first name": "Роман",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m7.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
	},
	{
		"first name": "Анастасія",
		"last name": "Гончарова",
		photo: "./img/trainers/trainer-f7.jpg",
		specialization: "Басейн",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
	},
	{
		"first name": "Валентин",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-m8.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
	},
	{
		"first name": "Лариса",
		"last name": "Петренко",
		photo: "./img/trainers/trainer-f8.jpg",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "7 років",
		description:
			"Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
	},
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m9.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "11 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f9.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m10.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Наталія",
		"last name": "Бондаренко",
		photo: "./img/trainers/trainer-f10.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "8 років",
		description:
			"Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
	},
	{
		"first name": "Андрій",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m11.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
	},
	{
		"first name": "Софія",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-f11.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "6 років",
		description:
			"Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
	},
	{
		"first name": "Дмитро",
		"last name": "Ковальчук",
		photo: "./img/trainers/trainer-m12.png",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
	},
	{
		"first name": "Олена",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-f12.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "5 років",
		description:
			"Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
	},
];


  
document.addEventListener('DOMContentLoaded', () => {

// loader
    const loaderDiv = document.createElement("div");
    loaderDiv.classList.add("loader");

    const headerElement = document.querySelector("header");
    headerElement.parentNode.insertBefore(loaderDiv, headerElement.nextSibling);

    setTimeout(function () {
    loaderDiv.parentNode.removeChild(loaderDiv);
    }, 3000);

// Дані про тренерів
    const trainersCards = document.querySelector('.trainers-cards__container');
    const template = document.getElementById('trainer-card');
    let filteredData = [...DATA];

// Функція рендерингу карточок тренерів
    function renderByTrainers(item) {
        trainersCards.innerHTML = '';
        item.forEach((trainer) => {
            const clone = template.content.cloneNode(true);

            const trainerImg = clone.querySelector('.trainer__img');
            trainerImg.src = trainer.photo;
            trainerImg.alt = `${trainer['first name']} ${trainer['last name']}`;

            const trainerName = clone.querySelector('.trainer__name');
            trainerName.textContent = `${trainer['first name']} ${trainer['last name']}`;

            const trainerButton = clone.querySelector('.trainer__show-more');
            trainerButton.addEventListener('click', () => {
                showModalWindow(trainer);
                document.body.style.overflow = 'hidden';
            });

            trainersCards.appendChild(clone);
        });
    }

// Функція для відображення модального вікна про тренера
    function showModalWindow(trainer) {
        const modalTemplate = document.getElementById('modal-template');
        const clone = modalTemplate.content.cloneNode(true);

        const modal = clone.querySelector('.modal');
        const modalImg = clone.querySelector('.modal__img');
        modalImg.src = trainer.photo;
        modalImg.alt = `${trainer['first name']} ${trainer['last name']}`;

        const modalName = clone.querySelector('.modal__name');
        modalName.textContent = `${trainer['first name']} ${trainer['last name']}`;

        const modalCategory = clone.querySelector('.modal__point--category');
        modalCategory.textContent = `Категорія: ${trainer.category}`;

        const modalExperience = clone.querySelector('.modal__point--experience');
        modalExperience.textContent = `Досвід: ${trainer.experience}`;

        const modalSpecialization = clone.querySelector(
        '.modal__point--specialization'
        );
        modalSpecialization.textContent = `Напрям тренера: ${trainer.specialization}`;

        const modalText = clone.querySelector('.modal__text');
        modalText.textContent = trainer.description;

        const closeButton = clone.querySelector('.modal__close');
        closeButton.addEventListener('click', () => {
            modal.remove();
        document.body.style.overflow = '';
        });
    
        document.body.appendChild(clone);
        modal.style.display = 'flex';
    }

//Ініціалізація відображення, сортування і фільтрації тренерів при загрузці сторінки
    function removeHiddenFromAll () {
        
        renderByTrainers(DATA);

        const sorting = document.querySelector('.sorting');
        sorting.removeAttribute('hidden');

        const sidebar = document.querySelector('.sidebar');
        sidebar.removeAttribute('hidden');
    };
    
    setTimeout(removeHiddenFromAll, 3000);
    

// Функції для сортування тренерів по різним критеріям
    
    const sortButtons = document.querySelectorAll('.sorting__btn');

    function sortByFirstName() {
        const sortedData = [...filteredData];
        sortedData.sort((a, b) =>
            a['last name'].localeCompare(b['last name'], 'uk')
        );
        renderByTrainers(sortedData);
    }

    function sortByExperience() {
        const sortedData = [...filteredData];
        sortedData.sort((a, b) => {
            const expA = parseInt(a.experience);
            const expB = parseInt(b.experience);
            return expB - expA;
        });
        renderByTrainers(sortedData);
    }

    function sortByDefault() {
        renderByTrainers(filteredData);
    }

// Функція події сортування
    function sortTrainers(el) {
        sortButtons.forEach((btn) => {
            btn.classList.remove('sorting__btn--active');
        });

        el.target.classList.add('sorting__btn--active');

        if (el.target.textContent.trim() === 'ЗА замовчуванням') {
            sortByDefault();
        } else if (el.target.textContent.trim() === 'ЗА ПРІЗВИЩЕМ') {
            sortByFirstName();
        } else if (el.target.textContent.trim() === 'ЗА ДОСВІДОМ') {
            sortByExperience();
        }
    }

    
    sortButtons.forEach((button) => {
        button.addEventListener('click', sortTrainers);
    });

// Функції для фільтрації тренерів за напрямком та категоріями
    function filterDirection(value, data) {
        switch (value) {
            case 'all':
                return data;
            case 'gym':
                return data.filter(
                    (elem) => elem.specialization === 'Тренажерний зал'
                );
            case 'fight club':
                return data.filter(
                    (elem) => elem.specialization === 'Бійцівський клуб'
                );
            case 'kids club':
                return data.filter(
                    (elem) => elem.specialization === 'Дитячий клуб'
                );
            case 'swimming pool':
                return data.filter((elem) => elem.specialization === 'Басейн');
            default:
                return data;
        }
    }

    function filterCategory(value, data) {
        switch (value) {
            case 'all':
                return data;
            case 'master':
                return data.filter((elem) => elem.category === 'майстер');
            case 'specialist':
                return data.filter((elem) => elem.category === 'спеціаліст');
            case 'instructor':
                return data.filter((elem) => elem.category === 'інструктор');
            default:
                return data;
        }
    }

// Функція для рендерингу тренерів по спеціалізації
    function renderBySpecialization() {
        const direction = document.querySelector(
            'input[name="direction"]:checked'
        ).value;
        const category = document.querySelector(
            'input[name="category"]:checked'
        ).value;

        filteredData = filterDirection(direction, DATA);
        filteredData = filterCategory(category, filteredData);

        renderByTrainers(filteredData);
    }

// Обробник подій для для фільтрації
    document
        .querySelector('.filters')
        .addEventListener('submit', function (event) {
            event.preventDefault();
            renderBySpecialization();
        });
});

